package com.tkoigra.tkoigra;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.logging.Handler;


public class MainWebView extends Activity  {

    public WebView mainWebView;
    private WebView newWebView;
    private static final String appUrl = "http://tkoigra.com";

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_web_view);

        context = getApplicationContext();

        mainWebView = (WebView) findViewById(R.id.webViewMain);
        //Add javascript interface
        //mainWebView.addJavascriptInterface(new WebAppInterface(this), "androidGoogleLogin");
        //Enable JavaScript
        WebSettings webSettings = mainWebView.getSettings();
        //For javascript to recognize android app
        webSettings.setUserAgentString("tkoigra_webview");
        webSettings.setJavaScriptEnabled(true);
        //Using HTML5 storage for session
        webSettings.setDomStorageEnabled(true);
        //Enable google login to open new window
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        //Javascript error logging
        mainWebView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d(" Tkoigra.com", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId() );
                return true;
            }

            public void onCloseWindow(WebView w){
                super.onCloseWindow(w);
                newWebView.setVisibility(WebView.GONE);
                Log.d(this.getClass().toString(), "Login window closed");
            }

            @Override
            public boolean onCreateWindow (WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                newWebView = new WebView(context);
                newWebView.getSettings().setJavaScriptEnabled(true);
                newWebView.setWebChromeClient(this);
                newWebView.setWebViewClient(new WebViewClient());
                RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
                mainLayout.addView(newWebView);
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }

        });
        //Open all links inside webview
        mainWebView.setWebViewClient(new WebViewClient());
        mainWebView.loadUrl(appUrl);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_web_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
    Javascript interface
    private class WebAppInterface{

        Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void reloadWebView() {
            runOnUiThread(new Runnable(){
                public void run() {
                    mainWebView.loadUrl("http://192.168.1.6:8080/nogomet/");
                }
            });
        };

    }
 */
}
